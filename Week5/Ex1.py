def primegen(N):
    result=[]
    if (N<=0):
        print("Wrong input!")
        return 0
    elif (N==1):
        result.append(1)
        return iter(result)
    elif (N==2):
        result.append(1)
        result.append(3)
    else:
        result.append(1)
        result.append(3)
        i=2
        while (i<=N):
            lastN=result[len(result)-1]
            while (True):
                lastN=lastN+1
                check=False
                for j in range(2,lastN-1):
                    if (lastN%j==0):
                        check=True
                if (check==False):
                    result.append(lastN)
                    break
            if (len(result)==N):
                return iter(result)
            i=i+1

prime=primegen(20)
print(next(prime))
print(next(prime))
print(next(prime))
print(next(prime))
print(next(prime))
print(prime)
