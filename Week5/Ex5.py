l = [2,4,-3,4.3,(3,5),[2,4,7]]
def mysumfunc(l):
    nl = [i if ((type(i) == int) or (type(i) == float)) else sum(i) for i in l]
    return(nl)

from functools import reduce

result=reduce(lambda x,y: x+y,mysumfunc(l))
print (result)