def countSeq(c,st):
    lenstring=len(st)
    count=0
    result=[]
    for i in range(lenstring):
        if (((count == 0) and (st[i] == c)) or ((count > 0) and (st[i - 1] == c) and (st[i] == c))):
            count=count+1
        elif (count>0):
            result.append(count)
            count=0
    return iter(result)

s= 'daaaddaaaaadsssssaaaaaaaddfffdaaaas'
dem=countSeq('a',s)
print(next(dem))
print(next(dem))
print(next(dem))
print(next(dem))