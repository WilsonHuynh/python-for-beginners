import os
def show(diretory, level):
    os.chdir(diretory)
    l=os.listdir(diretory)
    for f in l:
        if os.path.isfile(f):
            for i in range(level-1):
                print("|",end="\t")
            str="|--\t"+ f
            size=os.path.getsize(os.path.join(diretory,f))
            if size<1000:
                print(str.ljust(100-(level-1)*4," "), size, "  b")
            else:
                print(str.ljust(100 - (level - 1) * 4, " "), round(size/1000,2), "  Kb")
    diretories=[d for d in l if os.path.isdir(d)]
    if not diretories:
        return
    else:
        for d in diretories:
            for i in range(level-1):
                print("|", end="\t")
            print ("|\t",d)
            show(os.path.join(diretory,d),level+1)


path=input("Enter diretory: ")
level=1
show(path,level)
