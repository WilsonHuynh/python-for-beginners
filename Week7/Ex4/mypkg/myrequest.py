import urllib.request
import os
def takerequest(url):
    try:
        opener = urllib.request.FancyURLopener({})
        page = opener.open(url)
        p = os.getcwd()
        l = os.listdir(p)
        lengthlink = url.__len__()
        if url.find("html") >= 0:
            index = url.find("html")
            while index > 0:
                if url[index] == "/":
                    break
                index = index - 1
            filename = url[index + 1:url.find(".html") - 1]
        else:
            filename = "index"
        version = 1
        for f in l:
            if os.path.isfile(f):
                if f.find(filename) >= 0:
                    version = version + 1
        if version == 1:
            name = filename + ".html"
        else:
            name = filename + "_" + str(version - 1) + ".html"
        file = open(name, "wb+")
        file.write(page.read())
        file.close()
        page.close()
    except:
        print("Error link!!!")
    return