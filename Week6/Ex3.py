answer="no"
while True:
    try:
        p=input("Enter path of folder: ")
        import os
        l = os.listdir(p)
        if len(l) == 0:
            print("Empty folder!!!")
        else:
            choose="n"
            count = 1
            while True:
                os.system('cls')
                print("Files/Sub Folders at ", p, " : \n")
                if count==0 or (count>round(len(l))/10 and len(l)%10==0) or (count>round(len(l))/10+1 and len(l)%10!=0):
                    break
                else:
                    try:
                        for f in l[(count-1)*10:(count-1)*10+10]:
                            print(f)
                    except:
                        for f in l[(count-1)*10:]:
                            print(f)
                    choose=input("Enter n for next detail, p for previous detail: ")
                    if choose=="n":
                        count=count+1
                    elif choose=="p":
                        count=count-1
                    else:
                        break
        break
    except:
        print("Invalid directory!!!\n")
        answer=input("Do you want to continue? (Yes/No): ")
        if answer.lower()!="yes":
            break
