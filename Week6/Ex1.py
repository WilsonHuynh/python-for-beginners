#Part 1
def checknumber(l):
    result=True
    if type(l)==str:
        return False
    elif type(l)==list or type(l)==tuple:
        for i in range(len(l)):
            if checknumber(l[i])==False:
                result=False
                break
        return result
    else:
        return True

def sumdeep(l1):
    if (len(l1)==1):
        if checknumber(l1[0])==False:
            if (type(l1[0]) == list or type(l1[0]) == tuple):
                return sumdeep(l1[0])
            else:
                return 0
        elif (type(l1[0])==list or type(l1[0])==tuple):
            return sumdeep(l1[0])
        else:
            return l1[0]
    else:
        if checknumber(l1[0]) == False:
            if (type(l1[0]) == list or type(l1[0]) == tuple):
                return sumdeep(l1[0])+sumdeep(l1[1:])
            else:
                return sumdeep(l1[1:])
        elif (type(l1[0]) == list or type(l1[0]) == tuple):
            return sumdeep(l1[0])+sumdeep(l1[1:])
        else:
            return l1[0]+sumdeep(l1[1:])

l1=[3,4,'hello',(4.6,5.6), [3,7,2,2,[0.3,5],'text'],6,3,3.5]
print(sumdeep(l1))


#Part 2
import json
with open ('text.txt','r') as f:
    lobj=json.load(f)
    print(sumdeep(lobj))


#Part 3
l2=[]
l2.append(l1)
for i in range(len(lobj)):
    l2.append(lobj[i])
print(l2)
import pickle
mybytes = [120, 3, 255, 0, 100]
with open("l2.bin", "wb+") as mypicklefile:
    pickle.dump(l2, mypicklefile)

#Part 4
f=open('l2.bin','rb+')
data=pickle.load(f)
print(data)
def changedata(data):
    if checknumber(data)==True:
        return data
    elif type(data)==list or type(data)==tuple:
        if checknumber(data[0])==True:
            if type(data[0])==list or type(data)==tuple:
                return data[0] + changedata(data[1:])
            else:
                return [data[0]] + changedata(data[1:])
        elif type(data[0])==list or type(data)==tuple:
            return changedata(data[0]) + changedata(data[1:])
        else:
            return changedata(data[1:])
    else:
        return []

with open("l2.bin", "wb+") as mypicklefile:
    pickle.dump(changedata(data), mypicklefile)