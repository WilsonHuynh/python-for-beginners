import os
p=os.getcwd()
l=os.listdir()
if os.path.isdir(l[0]):
    print (l[0])

def show(l):
    if len(l)==1:
        if os.path.isfile(l[0]):
            return "f_"+ l[0]
        elif os.path.isdir(l[0]):
            return "d_"+l[0]
        else:
            return None
    elif len(l)==0:
        return "Empty"
    else:
        if os.path.isfile(l[0]):
            return "f_"+l[0]+"\n"+show(l[1:])
        elif os.path.isdir(l[0]):
            return "d_"+l[0]+"\n"+show(l[1:])
        else:
            return None

print("Files/Folders at ",p,":\n" ,show(l))