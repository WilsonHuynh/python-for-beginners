#  u, v, x, y, z= 29, 12, 10, 4, 3
# u/v            ===> 2.4166666666666665
# t=(u==v)       ===> false
# u%x            ===> 9
# t=(x>=y)       ===> true
# u+=5           ===> 34
# u%=z           ===> 2
# t=(v>x and y<z)===> false
# x**z           ===> 1000
# x//z           ===> 3

# Check
[u,v,x,y,z]=[29, 12, 10, 4, 3]
print("u/v = ", u/v)
print("u==v = ", u==v)
print("u%x = ", u%x)
print("x>=y = ", x>=y)
u+=5
print("u+=5 = ",u)
u=29
u%=z
print("u%=z = ", u)
print("(v>x and y<z) = ", (v>x and y<z))
print("x**z = ", x**z)
print("x//z = ", x//z)
