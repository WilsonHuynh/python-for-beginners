import os
import numpy as np
from io import StringIO

f=np.genfromtxt("iris.txt",dtype=None, names=["myfloat","myfloat","myfloat","myfloat","mystring"],delimiter=",")
data=[]
flower=[]
l=f[0]
for item in f:
    for i in range(4):
        data.append(item[i])
    if item[4]==b'Iris-setosa':
        flower.append(1)
    elif item[4]==b'Iris-versicolor':
        flower.append(2)
    else:
        flower.append(3)
observation=np.empty((len(f),4))
flowers=np.ones((len(f),1),dtype=int)
for i in range(len(f)):
    for j in range(4):
        observation[i,j]=data[i*4+j]
    flowers[i,0]=flowers[i,0]*int(flower[i])
#print(observation)
#print(flowers)
end=0
for i in range(1,4):
    start=end
    for j in range(len(flowers)):
        if flowers[j,0]==i:
            end=j
    if i==1:
        print("Covariance between sepal length and width of Iris-setosa: \n", np.cov(observation[start:end,0],observation[start:end,1]))
    elif i==2:
        print("Covariance between sepal length and width of Iris-versicolor: \n",np.cov(observation[start:end, 0], observation[start:end, 1]))
    else:
        print("Covariance between sepal length and width of Iris-virginica: \n",np.cov(observation[start:end, 0], observation[start:end, 1]))
