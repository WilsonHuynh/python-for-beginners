import numpy as np
#-------CASE--------
#So sanh duoc mat giua 2 phuong thuc mua nha Vincity:
#   Tra tien 100% can ho tri gia 790 trieu, duoc chiet khau 10%
#   Mua tra gop trong 35 nam, ngan hang cho vay 70%, tra hang thang 3.9 trieu
#++++Lai suat ngan hang la 9%/nam


#-------FV---------
rate=0.09/12
nper=35*12
value=790
fv_payall=np.fv(rate, nper,0,-value*0.9)
fv_payinstall=np.fv(rate, nper,-3.9,-value*0.3)
print("Chenh lech FV chi tra can ho sau 35 nam giua mua tra gop va mua lien la: ", int(fv_payinstall-fv_payall)," trieu dong")
#-------NP---------
pv_payall=value*0.9
pv_payinstall=np.pv(rate, nper, -3.9)+value*0.3
print("Chenh lech giua NP chi tra can ho giua mua tra gop va mua ngay la: ", int(pv_payinstall-pv_payall), " trieu dong")
#-------NPV------------
pay_install=[value*0.3]
for i in range(nper):
    pay_install.append(3.9)
pay_all=[value*0.9]
npv_install=np.npv(rate, np.array(pay_install))
npv_all=np.npv(rate, np.array(pay_all))
print("Chenh lech NPV giua tra gop va tra ngay la: ", int(npv_install-npv_all), "trieu dong")
#-------PMT------------
print("Muc tra hang thang de khong bi lo so voi tra ngay la: ",int(np.pmt(rate,nper,(0.9-0.3)*value))," trieu dong/thang")
#-------IRR-----------
l=[-value*(0.9-0.3)]
for i in range(nper):
    l.append(3.9)
print("IRR giua tra gop va tra ngay la: ", float(np.irr(np.array(l))*100),"%/thang")