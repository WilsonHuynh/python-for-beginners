import numpy as np
import random as rd
m=int(input("Enter the deep of array: "))
n=int(input("Enter the height of array: "))
k=int(input("Enter the width of array: "))
l=[]
for i in range(m*n*k):
    l.append(rd.randint(-100,100))
a=(np.array(l)).reshape((m,n,k))
print(a)
print("Sum by deep: \n",a.sum(axis=0))
print("Sum by height: \n",a.sum(axis=1))
print("Sum by width: \n",a.sum(axis=2))
