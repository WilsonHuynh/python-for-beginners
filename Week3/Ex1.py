# create sets
A={1,2,3,4,5,7}
B={2,4,5,9,12,24}
C={2,4,8}

#iterate over C and add to A and B and print out A & B
for item in C:
    A.add(item)
    B.add(item)
print(f'A= {A}')
print (f'B= {B}')

print(f'Intersection of A&B= {A.intersection(B)}')
print (f'Union of A&B= {A.union(B)}')
print(f'Item in A but not in B= {A.difference(B)}')
print (f'Length of A= {len(A)} and length of B= {len(B)}')
print (f'Max in union of A&B= {max(A.union(B))}')
print (f'Min in union of A&B= {min(A.union(B))}')