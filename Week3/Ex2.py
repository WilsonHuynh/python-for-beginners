t=(1, 'python', [2,3], (4,5))
t1,t2,t3,t4=t
t41,t42=t4
print(t1,t2,t3[0],t3[1],t41,t42)
print(f'The last item in t: {t4}')

#add item
t=list(t)
t.append([2,3])
t=tuple(t)
#check duplicate
dup=0
for item in t:
    if item==[2,3]:
        dup+=1
if (dup==2):
    print(f'[2,3] is duplicated')
else:
    print(f'[2,3] is not duplicated')

#remove [2,3]
t=list(t)
t.remove([2, 3])
t.remove([2,3])
t=tuple(t)
print (f'Tuple after remove: {t}')

print (f'Type of t after convert: {type(list(t))}')