dic1={1:10,2:20}
dic2={3:30,4:40}
dic3={5:50,6:60}
#concate into new_dic
new_dic=dic1
key2=dic2.keys()
key3=dic3.keys()
for key in key2:
    new_dic[key]=dic2[key]
for key in key3:
    new_dic[key]=dic3[key]
print (new_dic)

#print a dic has key from 1:15 and values are square of keys
dem=0
dic=dict()
while True:
    if dem==15:
        break
    dem+=1
    dic[dem]=dem*dem
print (f'Dict has key from 1:15 and values are square of keys: {dic}')

#sort ascending a dict by value
dict={'a':1,'b':4,'c':2}
vals=dict.values()
vals=list(vals)
vals.sort()
result=list()
for val in vals:
    for key in dict:
        if dict[key]==val:
            result.append(key)
print (f'Sort ascending a dict by value: {result}')

#String
s="Python is an easy language to learn"
d={}
for c in s:
    if c not in d:
        d[c]=1
    else:
        d[c]=d[c]+1
print (d)
