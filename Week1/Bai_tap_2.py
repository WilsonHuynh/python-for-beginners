
# coding: utf-8

# In[1]:


def sum(x, y): #Tu tao ham sum voi 2 doi so x va y duoc dua vao
    sum = x + y #bien sum bang tong cua x va y
    if sum in range(15, 20): #Neu sum nam trong khoang tu 15 den 20 thi tra ve gia tri 20 cho ham, neu khong thi tra ve gia tri bien sum
        return 20
    else:
        return sum

print(sum(10, 6)) #Ket qua se la 20
print(sum(10, 2)) #Ket qua se la 12
print(sum(10, 12)) #Ket qua se la 22

