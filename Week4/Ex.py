filedata=open('epldata_final.csv')
file=filedata.read()
datalist=file.split("\n")[1:-1]
data=[]
#Create list of data of players
for player in datalist:
    data.append(player.split(","))

#create list of unique club
clublist=[]
for player in data:
    if player[1] in clublist:
        continue
    else:
        clublist.append(player[1])
print(clublist)

#create a dict of palyers in each club
playersInClub={}
for player in clublist:
    playersInClub[player]=0
for player in data:
    playersInClub[player[1]]= playersInClub[player[1]]+1
print (playersInClub)

#Find the youngest and oldest player(s)
youngest=100
oldest=0
p_youngest=[]
p_oldest=[]
for player in data:
    if int(player[2])<youngest:
        youngest=int(player[2])
        p_youngest.append(player[0])
    if int(player[2])>oldest:
        oldest=int(player[2])
        p_oldest.append(player[0])
print ("The oldest player(s) is(are): ", p_oldest," - ", oldest," year-old")
print ("The youngest player(s) is(are): ", p_youngest," - ", youngest, " year-old")

#Find the average age
age=0
for player in data:
    age=age+int(player[2])
print ("The average age is: ", int(age/len(data)))

ageClub={}
for player in data:
    if player[1] not in ageClub:
        ageClub[player[1]]=int(player[2])
    else:
        ageClub[player[1]] = ageClub[player[1]]+int(player[2])
for player in ageClub:
    ageClub[player]=float(ageClub[player]/playersInClub[player])
print(ageClub)

#Sort by average age of each club
print(sorted(ageClub.items(), key=lambda x: x[1],reverse=True))


#Compute market value
totalbig6club=0
other=0
for player in data:
    if int(player[15])==1:
        totalbig6club=totalbig6club+float(player[5])
    else:
        other=other+float(player[5])
print("Total market value of players in 6 big clubs is: ",round(totalbig6club))
print("Total market value of another is: ", round(other))
